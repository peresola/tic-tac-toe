<?php
namespace App\TicTacToe\UI\Cli;

use App\TicTacToe\Domain\User\Application\CreateUserCommand;
use League\Tactician\CommandBus;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class CreateUserCliCommand extends Command
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        parent::__construct();
        $this->commandBus = $commandBus;
    }

    protected function configure()
    {
        $this
            ->setName('app:create-user')
            ->setDescription('Creates a new user.')
            ->setHelp('This command allows you to create a user...')
            ->addArgument('id', InputArgument::REQUIRED, 'The id of the user.')
            ->addArgument('username', InputArgument::REQUIRED, 'The username of the user.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $id = ($input->getArgument('id') ? : Uuid::uuid4()->toString());
            $username = $input->getArgument('username');

            $command = new CreateUserCommand($id,$username);
            $this->commandBus->handle($command);

            echo sprintf('User created with %s id and %s username' . PHP_EOL, $id ,$username);
            $output->write('User created');
        } catch (\Exception $e) {
            error_log($e->getMessage(), $e->getCode());
            exit(1);
        }
    }
}