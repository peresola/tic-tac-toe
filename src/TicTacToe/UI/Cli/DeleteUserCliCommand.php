<?php

namespace App\TicTacToe\UI\Cli;


use App\TicTacToe\Domain\User\Application\CreateUserCommand;
use App\TicTacToe\Domain\User\Application\DeleteUserCommand;
use League\Tactician\CommandBus;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class DeleteUserCliCommand extends Command
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        parent::__construct();
        $this->commandBus = $commandBus;
    }

    protected function configure()
    {
        $this
            ->setName('app:delete-user')
            ->setDescription('Deletes a user.')
            ->setHelp('This command allows you to delete a user...')
            ->addArgument('id', InputArgument::REQUIRED, 'The id of the user.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $id = $input->getArgument('id');

            $command = new DeleteUserCommand($id);
            $this->commandBus->handle($command);

            echo sprintf('User with %s id has been deleted' . PHP_EOL, $id);
            $output->write('User deleted');
        } catch (\Exception $e) {
            error_log($e->getMessage(), $e->getCode());
            exit(1);
        }
    }
}