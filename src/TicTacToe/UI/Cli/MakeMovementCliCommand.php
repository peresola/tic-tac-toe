<?php

namespace App\TicTacToe\UI\Cli;


use App\TicTacToe\Domain\Movement\Application\MakeMovementCommand;
use League\Tactician\CommandBus;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MakeMovementCliCommand extends Command
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        parent::__construct();
        $this->commandBus = $commandBus;
    }

    protected function configure()
    {
        $this
            ->setName('app:make-movement')
            ->setDescription('Makes a movement in a game.')
            ->setHelp('This command allows you to make a movement in a game...')
            ->addArgument('id', InputArgument::REQUIRED, 'The id of the movement.')
            ->addArgument('gameId', InputArgument::REQUIRED, 'The id of the game.')
            ->addArgument('playerId', InputArgument::REQUIRED, 'The id of the player.')
            ->addArgument('position', InputArgument::REQUIRED, 'The position of the movement.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $id = ($input->getArgument('id') ? : Uuid::uuid4()->toString());
            $gameId = ($input->getArgument('gameId') ? : Uuid::uuid4()->toString());
            $playerId = ($input->getArgument('playerId') ? : Uuid::uuid4()->toString());
            $position = $input->getArgument('position');

            $command = new MakeMovementCommand($id,$gameId,$playerId,$position);
            $this->commandBus->handle($command);

            echo sprintf('Movement done with %s id on %s gameId and %s playerId with %s position' . PHP_EOL, $id ,$gameId,$playerId,$position);
            $output->write('Movement done');
        } catch (\Exception $e) {
            error_log($e->getMessage(), $e->getCode());
            exit(1);
        }
    }
}