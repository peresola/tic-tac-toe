<?php

namespace App\TicTacToe\UI\Cli;


use App\TicTacToe\Domain\Game\Application\GameHasWinnerCommand;
use League\Tactician\CommandBus;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GameHasWinnerCliCommand extends Command
{

    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        parent::__construct();
        $this->commandBus = $commandBus;
    }

    protected function configure()
    {
        $this
            ->setName('app:game-has-winner')
            ->setDescription('Check if game has a winner.')
            ->setHelp('This command allows you to check if a game has a winner...')
            ->addArgument('gameId', InputArgument::REQUIRED, 'The id of the game.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $id = ($input->getArgument('gameId') ? : Uuid::uuid4()->toString());
            $command = new GameHasWinnerCommand($id);

            if($playerId = $this->commandBus->handle($command)){
                echo sprintf('Game has winner with %s playerId' . PHP_EOL, $playerId);
                $output->write('Game has winner');
            }else{
                echo sprintf('Game doesn\'t have a winner yet' . PHP_EOL);
                $output->write('Game doesn\'t have a winner yet');
            }


        } catch (\Exception $e) {
            error_log($e->getMessage(), $e->getCode());
            exit(1);
        }
    }
}