<?php

namespace App\TicTacToe\UI\Cli;


use App\TicTacToe\Domain\Game\Application\GameHasFinishedCommand;
use League\Tactician\CommandBus;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GameHasFinishedCliCommand extends Command
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        parent::__construct();
        $this->commandBus = $commandBus;
    }

    protected function configure()
    {
        $this
            ->setName('app:game-has-finished')
            ->setDescription('Check if game has finished.')
            ->setHelp('This command allows you to check if a game has finished...')
            ->addArgument('gameId', InputArgument::REQUIRED, 'The id of the game.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $id = ($input->getArgument('gameId') ? : Uuid::uuid4()->toString());

            $command = new GameHasFinishedCommand($id);
            if($this->commandBus->handle($command)){
                echo sprintf('Game %s has finished' . PHP_EOL, $id);
                $output->write('Game has finished');
            }else{
                echo sprintf('Game %s has not finished yet' . PHP_EOL, $id);
                $output->write('Game has not finished yet');
            }


        } catch (\Exception $e) {
            error_log($e->getMessage(), $e->getCode());
            exit(1);
        }
    }
}