<?php

namespace App\TicTacToe\UI\Cli;


use App\TicTacToe\Domain\Game\Application\StartGameCommand;
use League\Tactician\CommandBus;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class StartGameCliCommand extends Command
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        parent::__construct();
        $this->commandBus = $commandBus;
    }

    protected function configure()
    {
        $this
            ->setName('app:start-game')
            ->setDescription('Starts a new game.')
            ->setHelp('This command allows you to start a game...')
            ->addArgument('gameId', InputArgument::REQUIRED, 'The id of the game.')
            ->addArgument('playerId1', InputArgument::REQUIRED, 'The id of the player1.')
            ->addArgument('playerId2', InputArgument::REQUIRED, 'The id of the player2.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $id = ($input->getArgument('gameId') ? : Uuid::uuid4()->toString());
            $playerId1 = ($input->getArgument('playerId1') ? : Uuid::uuid4()->toString());
            $playerId2 = ($input->getArgument('playerId2') ? : Uuid::uuid4()->toString());

            $command = new StartGameCommand($id,$playerId1,$playerId2);
            $this->commandBus->handle($command);

            echo sprintf('Game started with %s id and %s playerId1 and %s playerId2' . PHP_EOL, $id ,$playerId1,$playerId2);
            $output->writeln('Game started');
        } catch (\Exception $e) {
            error_log($e->getMessage(), $e->getCode());
            exit(1);
        }
    }
}