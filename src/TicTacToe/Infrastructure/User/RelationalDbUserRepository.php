<?php

namespace App\TicTacToe\Infrastructure\User;


use App\TicTacToe\Domain\User\Domain\User;
use App\TicTacToe\Domain\User\Domain\UserId;
use App\TicTacToe\Domain\User\Domain\UserNotFoundException;
use App\TicTacToe\Domain\User\Infrastructure\UserRepository;

class RelationalDbUserRepository implements UserRepository
{

    function store(User $user): void
    {
        // TODO: Implement store() method.
    }

    function find(UserId $id): ?User
    {
        return null;
    }

    function findOrFail(UserId $id): User
    {
//        if (!$user = $this->find($id)) {
//            throw new UserNotFoundException(sprintf('User <%s> id not found', $id));
//        }
        $user = User::create($id,"");

        return $user;
    }

    function remove(User $user): void
    {
        // TODO: Implement remove() method.
    }
}