<?php

namespace App\TicTacToe\Infrastructure\Game;


use App\TicTacToe\Domain\Game\Domain\Game;
use App\TicTacToe\Domain\Game\Domain\GameId;
use App\TicTacToe\Domain\Game\Domain\PlayerId;
use App\TicTacToe\Domain\Game\Infrastructure\GameRepository;
use Ramsey\Uuid\Uuid;

class RelationalDbGameRepository implements GameRepository
{

    function store(Game $game): void
    {
        // TODO: Implement store() method.
    }

    /**
     * @param string $gameId
     * @return \App\TicTacToe\Domain\Game\Domain\Game|null
     * @throws \Exception
     */
    function findOrFail(string $gameId): ?Game
    {
        return Game::start(GameId::fromString($gameId),PlayerId::fromString(Uuid::uuid4()->toString()),PlayerId::fromString(Uuid::uuid4()->toString()));
    }
}