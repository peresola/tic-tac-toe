<?php

namespace App\TicTacToe\Domain\User\Application;


use App\TicTacToe\Domain\User\Domain\User;
use App\TicTacToe\Domain\User\Domain\UserCreateException;
use App\TicTacToe\Domain\User\Domain\UserId;
use App\TicTacToe\Domain\User\Infrastructure\UserRepository;

class CreateUserHandler
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * CreateUserHandler constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function handle(CreateUserCommand $createUserCommand):void
    {
        try {
            $this->userRepository->store(User::create(UserId::fromString($createUserCommand->getId()), $createUserCommand->getUsername()));
        } catch (UserCreateException $e) {
        }
    }
}