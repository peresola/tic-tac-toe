<?php

namespace App\TicTacToe\Domain\User\Application;


use App\TicTacToe\Domain\User\Domain\UserDeleteException;
use App\TicTacToe\Domain\User\Domain\UserId;
use App\TicTacToe\Domain\User\Domain\UserNotFoundException;
use App\TicTacToe\Domain\User\Infrastructure\UserRepository;

class DeleteUserHandler
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * CreateUserHandler constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function handle(DeleteUserCommand $createUserCommand):void
    {
        try {
            $user = $this->userRepository->findOrFail(UserId::fromString($createUserCommand->getId()));
            $this->userRepository->remove($user);
        }catch (UserNotFoundException $e){

        }catch (UserDeleteException $e){

        }
    }
}