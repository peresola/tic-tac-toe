<?php

namespace App\TicTacToe\Domain\User\Application;

use App\TicTacToe\Application\Command\TicTacToeCommand;

class DeleteUserCommand implements TicTacToeCommand
{
    /**
     * @var string
     */
    private $id;

    /**
     * DeleteUserCommand constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }


}