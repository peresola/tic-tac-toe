<?php

namespace App\TicTacToe\Domain\User\Application;

use App\TicTacToe\Application\Command\TicTacToeCommand;

final class CreateUserCommand implements TicTacToeCommand
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    public function __construct(string $id, string $username)
    {
        $this->id = $id;
        $this->username = $username;
    }

    public function getId() : string
    {
        return $this->id;
    }

    public function getUsername() : string
    {
        return $this->username;
    }
}