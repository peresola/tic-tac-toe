<?php

namespace App\TicTacToe\Domain\User\Infrastructure;


use App\TicTacToe\Domain\User\Domain\User;
use App\TicTacToe\Domain\User\Domain\UserId;

interface UserRepository
{
    public function store(User $user):void;

    public function find(UserId $id): ?User;

    public function findOrFail(UserId $id): User;

    public function remove(User $user): void;
}