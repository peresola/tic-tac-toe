<?php

namespace App\TicTacToe\Domain\User\Domain;

final class User
{
    /**
     * @var UserId $id
     */
    private $id;

    /**
     * @var string $username
     */
    private $username;

    /**
     * User constructor.
     * @param UserId $id
     * @param $username
     */
    private function __construct(UserId $id, $username)
    {
        $this->id = $id;
        $this->username = $username;
    }

    public static function create(UserId $userId, string $username):User
    {
        return new self($userId,$username);
    }

    /**
     * @return UserId
     */
    public function getId(): UserId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

}