<?php

namespace App\TicTacToe\Domain\User\Domain;

use Assert\Assertion;

final class UserId
{

    private $id;

    private function __construct(string $id)
    {
        Assertion::uuid($id);
        $this->id = $id;
    }

    public static function fromString(string $id): UserId
    {
        return new self($id);
    }
}