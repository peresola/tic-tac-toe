<?php

namespace App\TicTacToe\Domain\Game\Application;


use App\TicTacToe\Domain\Game\Domain\PlayerId;
use App\TicTacToe\Domain\Game\Infrastructure\GameRepository;

class GameHasWinnerHandler
{
    /**
     * @var GameRepository
     */
    private $gameRepository;

    /**
     * GameHasWinnerHandler constructor.
     * @param GameRepository $gameRepository
     */
    public function __construct(GameRepository $gameRepository)
    {
        $this->gameRepository = $gameRepository;
    }

    public function handle(GameHasWinnerCommand $gameHasWinnerCommand):?PlayerId
    {
        $game = $this->gameRepository->findOrFail($gameHasWinnerCommand->getGameId());
        return $game->getWinner();
    }

}