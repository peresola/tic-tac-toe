<?php

namespace App\TicTacToe\Domain\Game\Application;


use App\TicTacToe\Application\Query\TicTacToeQuery;

class GameHasWinnerCommand implements TicTacToeQuery
{
    /**
     * @var string
     */
    private $gameId;

    /**
     * GameHasWinnerCommand constructor.
     * @param string $gameId
     */
    public function __construct(string $gameId)
    {
        $this->gameId = $gameId;
    }

    /**
     * @return string
     */
    public function getGameId(): string
    {
        return $this->gameId;
    }

}