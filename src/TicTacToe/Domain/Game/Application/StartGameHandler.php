<?php

namespace App\TicTacToe\Domain\Game\Application;


use App\TicTacToe\Domain\Game\Domain\Game;
use App\TicTacToe\Domain\Game\Domain\GameId;
use App\TicTacToe\Domain\Game\Domain\PlayerId;
use App\TicTacToe\Domain\Game\Infrastructure\GameRepository;
use App\TicTacToe\Domain\User\Domain\UserId;
use App\TicTacToe\Domain\User\Infrastructure\UserRepository;

class StartGameHandler
{
    /** @var GameRepository */
    private $gameRepository;

    /** @var UserRepository */
    private $userRepository;

    /**
     * StartGameHandler constructor.
     * @param GameRepository $gameRepository
     * @param UserRepository $userRepository
     */
    public function __construct(GameRepository $gameRepository, UserRepository $userRepository)
    {
        $this->gameRepository = $gameRepository;
        $this->userRepository = $userRepository;
    }

    public function handle(StartGameCommand $startGameCommand):void
    {
        $this->checkPlayersExists($startGameCommand->getPlayerId1(),$startGameCommand->getPlayerId2());

        $gameId = GameId::fromString($startGameCommand->getId());
        $playerId1 = PlayerId::fromString($startGameCommand->getPlayerId1());
        $playerId2 = PlayerId::fromString($startGameCommand->getPlayerId2());

        $game = Game::start($gameId,$playerId1,$playerId2);
        $this->gameRepository->store($game);
    }

    private function checkPlayersExists(string $getPlayerId1, string $getPlayerId2)
    {
        $this->userRepository->findOrFail(UserId::fromString($getPlayerId1));
        $this->userRepository->findOrFail(UserId::fromString($getPlayerId2));
    }


}