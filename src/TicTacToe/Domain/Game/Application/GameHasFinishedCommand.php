<?php

namespace App\TicTacToe\Domain\Game\Application;


use App\TicTacToe\Application\Query\TicTacToeQuery;

class GameHasFinishedCommand implements TicTacToeQuery
{
    /**
     * @var string
     */
    private $gameId;

    /**
     * GameHasFinishedCommand constructor.
     * @param string $gameId
     */
    public function __construct(string $gameId)
    {
        $this->gameId = $gameId;
    }

    /**
     * @return string
     */
    public function getGameId(): string
    {
        return $this->gameId;
    }


}