<?php

namespace App\TicTacToe\Domain\Game\Application;


use App\TicTacToe\Domain\Game\Infrastructure\GameRepository;

class GameHasFinishedHandler
{
    /**
     * @var GameRepository
     */
    private $gameRepository;

    /**
     * GameHasFinishedHandler constructor.
     * @param GameRepository $gameRepository
     */
    public function __construct(GameRepository $gameRepository)
    {
        $this->gameRepository = $gameRepository;
    }

    public function handle(GameHasFinishedCommand $gameHasFinishedCommand):bool
    {
        $game = $this->gameRepository->findOrFail($gameHasFinishedCommand->getGameId());
        return $game->isFinished();
    }
}