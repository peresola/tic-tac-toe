<?php

namespace App\TicTacToe\Domain\Game\Application;

use App\TicTacToe\Application\Command\TicTacToeCommand;

class StartGameCommand implements TicTacToeCommand
{
    /** @var string */
    private $id;

    /** @var string */
    private $playerId1;

    /** @var string */
    private $playerId2;

    /**
     * StartGameCommand constructor.
     * @param string $id
     * @param string $playerId1
     * @param string $playerId2
     */
    public function __construct(string $id, string $playerId1, string $playerId2)
    {
        $this->id = $id;
        $this->playerId1 = $playerId1;
        $this->playerId2 = $playerId2;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPlayerId1(): string
    {
        return $this->playerId1;
    }

    /**
     * @return string
     */
    public function getPlayerId2(): string
    {
        return $this->playerId2;
    }


}