<?php

namespace App\TicTacToe\Domain\Game\Domain;


class MaximumGameMovementsReached extends \DomainException
{

    /**
     * MaximumGameMovementsReached constructor.
     */
    public function __construct()
    {
    }
}