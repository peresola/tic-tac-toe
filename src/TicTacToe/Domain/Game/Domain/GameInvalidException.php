<?php

namespace App\TicTacToe\Domain\Game\Domain;


class GameInvalidException extends \DomainException
{

    /**
     * GameInvalidException constructor.
     */
    public function __construct()
    {
    }
}