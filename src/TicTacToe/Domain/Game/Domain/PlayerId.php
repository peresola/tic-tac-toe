<?php

namespace App\TicTacToe\Domain\Game\Domain;

use Assert\Assertion;

class PlayerId
{
    /**
     * @var string
     */
    private $id;

    private function __construct(string $id)
    {
        Assertion::uuid($id);
        $this->id = $id;
    }

    /**
     * @param string $id
     * @return \App\TicTacToe\Domain\Game\Domain\PlayerId
     */
    public static function fromString(string $id): PlayerId
    {
        return new self($id);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}