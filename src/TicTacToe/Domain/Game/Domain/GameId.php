<?php

namespace App\TicTacToe\Domain\Game\Domain;

use Assert\Assertion;

class GameId
{
    /**
     * @var string
     */
    private $id;

    private function __construct(string $id)
    {
        Assertion::uuid($id);
        $this->id = $id;
    }

    public static function fromString(string $id): GameId
    {
        return new self($id);
    }
}