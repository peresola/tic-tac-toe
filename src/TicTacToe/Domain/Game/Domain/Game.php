<?php

namespace App\TicTacToe\Domain\Game\Domain;


use App\TicTacToe\Domain\Movement\Domain\Movement;
use App\TicTacToe\Domain\Movement\Domain\Position;
use Assert\Assertion;

class Game
{
    private const MAX_MOVEMENTS = 8;

    private const WINNING_COMBINATIONS = [[0,1,2],[3,4,5],[6,7,8],[0,4,8],[2,4,6],[0,3,6],[1,4,7],[2,5,8]];
    /**
     * @var GameId
     */
    private $id;

    /**
     * @var PlayerId
     */
    private $playerId1;

    /**
     * @var PlayerId
     */
    private $playerId2;

    /**
     * @var Movement[]
     */
    private $movements = [];

    /**
     * @var PlayerId
     */
    private $winner = null;

    /**
     * @var bool
     */
    private $isFinished = false;

    /**
     * Game constructor.
     * @param GameId $id
     * @param PlayerId $playerId1
     * @param PlayerId $playerId2
     */
    private function __construct(GameId $id, PlayerId $playerId1, PlayerId $playerId2)
    {
        Assertion::notEq($playerId1->getId(),$playerId2->getId());
        $this->id = $id;
        $this->playerId1 = $playerId1;
        $this->playerId2 = $playerId2;
    }

    /**
     * @param GameId $id
     * @param PlayerId $playerId1
     * @param PlayerId $playerId2
     * @return Game
     */
    public static function start(GameId $id, PlayerId $playerId1,PlayerId $playerId2):Game
    {
        return new self($id,$playerId1,$playerId2);
    }

    /**
     * @return GameId
     */
    public function getId(): GameId
    {
        return $this->id;
    }

    /**
     * @return PlayerId
     */
    public function getPlayerId1(): PlayerId
    {
        return $this->playerId1;
    }

    /**
     * @return PlayerId
     */
    public function getPlayerId2(): PlayerId
    {
        return $this->playerId2;
    }

    /**
     * @return PlayerId|null
     */
    public function getWinner(): ?PlayerId
    {
        return $this->winner;
    }

    /**
     * @return bool
     */
    public function isFinished(): bool
    {
        return $this->isFinished;
    }

    public function playMovement(Movement $movement):void
    {
        $this->isValidMove($movement);
        $this->movements[] = $movement;
        $this->isGameWonByMovement($movement);
    }

    /**
     * @param Movement $movement
     */
    private function isValidMove(Movement $movement):void
    {
        $this->validateMaximumMovements();
        $this->validateMovementPlayer($movement->getPlayerId());
        $this->validateMovementGame($movement->getGameId());
        $this->validateMovementPosition($movement->getPosition());
        $this->validateLastMovementIsNotFromSamePlayer($movement->getPlayerId());
    }

    private function isGameWonByMovement(Movement $movement)
    {
        try{
            $this->validateMaximumMovements();
        }catch (MaximumGameMovementsReached $e){
            $this->isFinished = true;
        }
        $this->isWinnerMovement($movement);
    }

    /**
     * @param PlayerId $getPlayerId
     */
    private function validateMovementPlayer(PlayerId $getPlayerId):void
    {
        if($getPlayerId !== $this->getPlayerId1() && $getPlayerId !== $this->getPlayerId2()){
            throw new PlayerInvalidException();
        }
    }

    /**
     * @param GameId $getGameId
     */
    private function validateMovementGame(GameId $getGameId):void
    {
        if($getGameId !== $this->getId()){
            throw new GameInvalidException();
        }
    }

    /**
     * @param Position $getPosition
     */
    private function validateMovementPosition(Position $getPosition):void
    {
        foreach ($this->movements as $index => $movement) {
            if($movement->getPosition()->getValue() === $getPosition->getValue()){
                throw new PositionInvalidException();
            }
        }
    }

    /**
     * @param PlayerId $getPlayerId
     */
    private function validateLastMovementIsNotFromSamePlayer(PlayerId $getPlayerId):void
    {
        if(!$this->movements) return;

        $lastMovement = end($this->movements);
        if($lastMovement->getPlayerId() === $getPlayerId){
            throw new InvalidTurnPlayerException();
        }
    }

    private function validateMaximumMovements()
    {
        if(count($this->movements)>=self::MAX_MOVEMENTS){
            throw new MaximumGameMovementsReached();
        }
    }

    /**
     * Checks if a movement makes to win the game
     * @param Movement $movement
     */
    private function isWinnerMovement(Movement $movement)
    {
        $playerMovements = $this->getPlayerMovements($movement->getPlayerId());
        if(count($playerMovements)<=2) return;
        foreach (self::WINNING_COMBINATIONS as $winningCombination){
            if(count(array_intersect($winningCombination, $playerMovements)) == count($winningCombination)){
                $this->isFinished = true;
                $this->winner = $movement->getPlayerId();
                return;
            }
        }
    }

    /**
     * Returns an array of the playerId movements
     * @param PlayerId $getPlayerId
     * @return array
     */
    private function getPlayerMovements(PlayerId $getPlayerId):array
    {
        $playerMovements = [];
        foreach ($this->movements as $movement){
            if($movement->getPlayerId() === $getPlayerId){
                $playerMovements[] = $movement->getPosition()->getValue();
            }
        }
        return $playerMovements;
    }


}