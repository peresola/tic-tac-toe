<?php

namespace App\TicTacToe\Domain\Game\Domain;


class PositionInvalidException extends \DomainException
{

    /**
     * PositionInvalid constructor.
     */
    public function __construct()
    {
    }
}