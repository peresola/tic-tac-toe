<?php
namespace App\TicTacToe\Domain\Game\Domain;


class PlayerInvalidException extends \DomainException
{

    /**
     * PlayerInvalidException constructor.
     */
    public function __construct()
    {
    }
}