<?php

namespace App\TicTacToe\Domain\Game\Domain;


class InvalidTurnPlayerException extends \DomainException
{

    /**
     * InvalidTurnPlayerException constructor.
     */
    public function __construct()
    {
    }
}