<?php

namespace App\TicTacToe\Domain\Game\Infrastructure;


use App\TicTacToe\Domain\Game\Domain\Game;

interface GameRepository
{

    function store(Game $game):void;

    function findOrFail(string $gameId):?Game;
}