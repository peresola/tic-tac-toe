<?php

namespace App\TicTacToe\Domain\Movement\Infrastructure;


use App\TicTacToe\Domain\Movement\Domain\Movement;

interface MovementRepository
{
    function store(Movement $movement):void ;
}