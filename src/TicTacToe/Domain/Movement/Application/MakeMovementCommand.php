<?php

namespace App\TicTacToe\Domain\Movement\Application;


use App\TicTacToe\Application\Command\TicTacToeCommand;

class MakeMovementCommand implements TicTacToeCommand
{
    /** @var string */
    private $id;

    /** @var string */
    private $gameId;

    /** @var string */
    private $playerId;

    /** @var int */
    private $position;

    /**
     * MakeMovementCommand constructor.
     * @param string $id
     * @param string $gameId
     * @param string $playerId
     * @param int $position
     */
    public function __construct(string $id, string $gameId, string $playerId, int $position)
    {
        $this->id = $id;
        $this->gameId = $gameId;
        $this->playerId = $playerId;
        $this->position = $position;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getGameId(): string
    {
        return $this->gameId;
    }

    /**
     * @return string
     */
    public function getPlayerId(): string
    {
        return $this->playerId;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }


}