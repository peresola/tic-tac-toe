<?php

namespace App\TicTacToe\Domain\Movement\Application;


use App\TicTacToe\Domain\Game\Domain\Game;
use App\TicTacToe\Domain\Game\Domain\GameId;
use App\TicTacToe\Domain\Game\Domain\PlayerId;
use App\TicTacToe\Domain\Game\Infrastructure\GameRepository;
use App\TicTacToe\Domain\Movement\Domain\Movement;
use App\TicTacToe\Domain\Movement\Domain\MovementId;
use App\TicTacToe\Domain\Movement\Domain\Position;
use App\TicTacToe\Domain\Movement\Infrastructure\MovementRepository;

class MakeMovementHandler
{
    /** @var MovementRepository */
    private $movementRepository;

    /** @var GameRepository */
    private $gameRepository;

    /**
     * MakeMovementHandler constructor.
     * @param MovementRepository $movementRepository
     * @param GameRepository $gameRepository
     */
    public function __construct(MovementRepository $movementRepository, GameRepository $gameRepository)
    {
        $this->movementRepository = $movementRepository;
        $this->gameRepository = $gameRepository;
    }

    public function handle(MakeMovementCommand $makeMovementCommand):void
    {
        $game = $this->checkGameExists($makeMovementCommand->getGameId());

        $movementId = MovementId::fromString($makeMovementCommand->getId());
        $gameId = GameId::fromString($makeMovementCommand->getGameId());
        $playerId = PlayerId::fromString($makeMovementCommand->getPlayerId());

        //$movement = Movement::create($movementId,$gameId,$playerId,new Position($makeMovementCommand->getPosition()));
        $movement = Movement::create($movementId,$game->getId(),$game->getPlayerId1(),new Position($makeMovementCommand->getPosition()));

        $game->playMovement($movement);
        $this->movementRepository->store($movement);
    }

    /**
     * @param string $gameId
     * @return Game|null
     */
    private function checkGameExists(string $gameId):?Game
    {
        return $this->gameRepository->findOrFail($gameId);
    }


}