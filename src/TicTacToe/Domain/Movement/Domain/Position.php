<?php

namespace App\TicTacToe\Domain\Movement\Domain;


use Assert\Assertion;

class Position
{
    /** @var int */
    private $value;

    const MIN_VALUE = 0;

    const MAX_VALUE = 8;

    /**
     * Position constructor.
     * @param int $value
     */
    public function __construct(int $value)
    {
        Assertion::between($value,self::MIN_VALUE,self::MAX_VALUE);
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

}