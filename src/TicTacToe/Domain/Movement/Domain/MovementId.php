<?php

namespace App\TicTacToe\Domain\Movement\Domain;


use Assert\Assertion;

class MovementId
{
    /**
     * @var string
     */
    private $id;

    private function __construct(string $id)
    {
        Assertion::uuid($id);
        $this->id = $id;
    }

    public static function fromString(string $id): MovementId
    {
        return new self($id);
    }
}