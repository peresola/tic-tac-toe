<?php

namespace App\TicTacToe\Domain\Movement\Domain;


use App\TicTacToe\Domain\Game\Domain\GameId;
use App\TicTacToe\Domain\Game\Domain\PlayerId;
use Assert\Assertion;

class Movement
{
    private const MINVALUE = 0;
    private const MAXVALUE = 8;
    /**
     * @var MovementId
     */
    private $id;

    /**
     * @var GameId
     */
    private $gameId;

    /**
     * @var PlayerId
     */
    private $playerId;

    /**
     * @var Position
     */
    private $position;

    /**
     * Movement constructor.
     * @param MovementId $id
     * @param GameId $gameId
     * @param PlayerId $playerId
     * @param Position $position
     */
    private function __construct(MovementId $id, GameId $gameId, PlayerId $playerId, Position $position)
    {
        $this->id = $id;
        $this->gameId = $gameId;
        $this->playerId = $playerId;
        $this->position = $position;
    }

    /**
     * @param MovementId $id
     * @param GameId $gameId
     * @param PlayerId $playerId
     * @param Position $position
     * @return Movement
     */
    public static function create(MovementId $id, GameId $gameId, PlayerId $playerId, Position $position):Movement
    {
        self::checkPositionIsValid($position);
        return new self($id,$gameId,$playerId,$position);
    }

    /**
     * @param Position $position
     */
    private static function checkPositionIsValid(Position $position)
    {
        Assertion::between($position->getValue(),self::MINVALUE,self::MAXVALUE);
    }

    /**
     * @return MovementId
     */
    public function getId(): MovementId
    {
        return $this->id;
    }

    /**
     * @return GameId
     */
    public function getGameId(): GameId
    {
        return $this->gameId;
    }

    /**
     * @return PlayerId
     */
    public function getPlayerId(): PlayerId
    {
        return $this->playerId;
    }

    /**
     * @return Position
     */
    public function getPosition(): Position
    {
        return $this->position;
    }
}