<?php

namespace App\Tests\Application\Game;

use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class GameStartApplicationTest extends KernelTestCase
{
    public function testExecute()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('app:start-game');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'  => $command->getName(),
            'gameId'=> Uuid::uuid4()->toString(),
            'playerId1' => Uuid::uuid4()->toString(),
            'playerId2' => Uuid::uuid4()->toString(),
        ]);

        $output = $commandTester->getDisplay();
        $this->assertContains('Game started', $output);
    }
}