<?php

namespace App\Tests\Application\Game;

use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class GameHasNotFinishedApplicationTest extends KernelTestCase
{
    public function testExecute()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('app:game-has-finished');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'  => $command->getName(),
            'gameId'=> Uuid::uuid4()->toString(),
        ]);

        $output = $commandTester->getDisplay();
        $this->assertContains('has not finished yet', $output);
    }
}