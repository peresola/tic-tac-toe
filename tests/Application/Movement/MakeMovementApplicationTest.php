<?php

namespace App\Tests\Application\Movement;


use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class MakeMovementApplicationTest extends KernelTestCase
{
    public function testExecute()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('app:make-movement');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'  => $command->getName(),
            'id'=> Uuid::uuid4()->toString(),
            'gameId'=> Uuid::uuid4()->toString(),
            'playerId'=> Uuid::uuid4()->toString(),
            'position'=> 5,
        ]);

        $output = $commandTester->getDisplay();
        $this->assertContains('Movement done', $output);
    }
}