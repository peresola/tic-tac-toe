<?php

namespace App\Tests\Application\User;

use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class CreateUserApplicationTest extends KernelTestCase
{
    public function testExecute()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('app:create-user');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'  => $command->getName(),
            'id'=> Uuid::uuid4()->toString(),
            'username'=> 'peresola',
        ]);

        $output = $commandTester->getDisplay();
        $this->assertContains('User created', $output);
    }
}