<?php

namespace App\Tests\Domain\Game;


use App\TicTacToe\Domain\Game\Domain\Game;
use App\TicTacToe\Domain\Game\Domain\GameId;
use App\TicTacToe\Domain\Game\Domain\PlayerId;
use App\TicTacToe\Domain\Movement\Domain\Movement;
use App\TicTacToe\Domain\Movement\Domain\MovementId;
use App\TicTacToe\Domain\Movement\Domain\Position;
use Assert\InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class GameTest extends TestCase
{
    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    public function invalid_game_when_passing_wrong_identifier():void
    {
        Game::start(GameId::fromString('invalid game id'),PlayerId::fromString(Uuid::uuid4()->toString()),PlayerId::fromString(Uuid::uuid4()->toString()));
    }

    /**
     * @test
     * @expectedException App\TicTacToe\Domain\Game\Domain\GameInvalidException
     */
    public function invalid_movement_is_not_from_a_game():void
    {
        $playerId = PlayerId::fromString(Uuid::uuid4()->toString());
        $game = Game::start(GameId::fromString(Uuid::uuid4()->toString()),PlayerId::fromString(Uuid::uuid4()->toString()),$playerId);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),GameId::fromString(Uuid::uuid4()->toString()),$playerId,new Position(3));
        $game->playMovement($movement);
    }

    /**
     * @test
     * @expectedException App\TicTacToe\Domain\Game\Domain\PlayerInvalidException
     */
    public function invalid_player_is_not_from_a_game():void
    {
        $gameId = GameId::fromString(Uuid::uuid4()->toString());
        $game = Game::start(GameId::fromString(Uuid::uuid4()->toString()),PlayerId::fromString(Uuid::uuid4()->toString()),PlayerId::fromString(Uuid::uuid4()->toString()));
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,PlayerId::fromString(Uuid::uuid4()->toString()),new Position(3));
        $game->playMovement($movement);
    }

    /**
     * @test
     * @expectedException App\TicTacToe\Domain\Game\Domain\PositionInvalidException
     */
    public function invalid_position_already_taken():void
    {
        $gameId = GameId::fromString(Uuid::uuid4()->toString());
        $playerId1 = PlayerId::fromString(Uuid::uuid4()->toString());
        $playerId2 = PlayerId::fromString(Uuid::uuid4()->toString());
        $game = Game::start($gameId,$playerId1,$playerId2);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId1,new Position(3));
        $game->playMovement($movement);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId2,new Position(3));
        $game->playMovement($movement);
    }

    /**
     * @test
     * @expectedException App\TicTacToe\Domain\Game\Domain\InvalidTurnPlayerException
     */
    public function invalid_turn_player():void
    {
        $gameId = GameId::fromString(Uuid::uuid4()->toString());
        $playerId1 = PlayerId::fromString(Uuid::uuid4()->toString());
        $playerId2 = PlayerId::fromString(Uuid::uuid4()->toString());
        $game = Game::start($gameId,$playerId1,$playerId2);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId1,new Position(3));
        $game->playMovement($movement);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId1,new Position(5));
        $game->playMovement($movement);
    }

    /**
     * @test
     * @expectedException App\TicTacToe\Domain\Game\Domain\MaximumGameMovementsReached
     */
    public function maximum_turns_played():void
    {
        $gameId = GameId::fromString(Uuid::uuid4()->toString());
        $playerId1 = PlayerId::fromString(Uuid::uuid4()->toString());
        $playerId2 = PlayerId::fromString(Uuid::uuid4()->toString());
        $game = Game::start($gameId,$playerId1,$playerId2);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId1,new Position(3));
        $game->playMovement($movement);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId2,new Position(5));
        $game->playMovement($movement);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId1,new Position(4));
        $game->playMovement($movement);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId2,new Position(0));
        $game->playMovement($movement);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId1,new Position(1));
        $game->playMovement($movement);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId2,new Position(2));
        $game->playMovement($movement);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId1,new Position(8));
        $game->playMovement($movement);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId2,new Position(6));
        $game->playMovement($movement);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId1,new Position(7));
        $game->playMovement($movement);
    }

    /**
     * @test
     */
    public function win_game_scenario()
    {
        $gameId = GameId::fromString(Uuid::uuid4()->toString());
        $playerId1 = PlayerId::fromString(Uuid::uuid4()->toString());
        $playerId2 = PlayerId::fromString(Uuid::uuid4()->toString());
        $game = Game::start($gameId,$playerId1,$playerId2);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId1,new Position(0));
        $game->playMovement($movement);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId2,new Position(5));
        $game->playMovement($movement);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId1,new Position(1));
        $game->playMovement($movement);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId2,new Position(3));
        $game->playMovement($movement);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId1,new Position(2));
        $game->playMovement($movement);

        $this->assertNotNull($game->getWinner());
        $this->assertTrue($game->isFinished());
    }

    /**
     * @test
     */
    public function game_is_finished_but_no_winner()
    {
        $gameId = GameId::fromString(Uuid::uuid4()->toString());
        $playerId1 = PlayerId::fromString(Uuid::uuid4()->toString());
        $playerId2 = PlayerId::fromString(Uuid::uuid4()->toString());
        $game = Game::start($gameId,$playerId1,$playerId2);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId1,new Position(0));
        $game->playMovement($movement);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId2,new Position(5));
        $game->playMovement($movement);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId1,new Position(1));
        $game->playMovement($movement);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId2,new Position(3));
        $game->playMovement($movement);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId1,new Position(8));
        $game->playMovement($movement);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId2,new Position(7));
        $game->playMovement($movement);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId1,new Position(6));
        $game->playMovement($movement);
        $movement = Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),$gameId,$playerId2,new Position(2));
        $game->playMovement($movement);


        $this->assertNull($game->getWinner());
        $this->assertTrue($game->isFinished());
    }


}