<?php

namespace App\Tests\Domain\User;


use App\TicTacToe\Domain\User\Domain\User;
use App\TicTacToe\Domain\User\Domain\UserId;
use Assert\InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    public function invalid_user_when_passing_wrong_id()
    {
        User::create(UserId::fromString('wrong id'),'pere');
    }
}