<?php

namespace App\Tests\Domain\Movement;


use App\TicTacToe\Domain\Game\Domain\GameId;
use App\TicTacToe\Domain\Game\Domain\PlayerId;
use App\TicTacToe\Domain\Movement\Domain\Movement;
use App\TicTacToe\Domain\Movement\Domain\MovementId;
use App\TicTacToe\Domain\Movement\Domain\Position;
use Assert\InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class MovementTest extends TestCase
{
    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    public function invalid_movement_when_passing_wrong_movement_id():void
    {
        Movement::create(MovementId::fromString('this is an invalid id'),GameId::fromString(Uuid::uuid4()->toString()),PlayerId::fromString(Uuid::uuid4()->toString()),new Position(4));
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    public function invalid_movement_when_passing_wrong_game_id():void
    {
        Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),GameId::fromString('wrong id'),PlayerId::fromString(Uuid::uuid4()->toString()),new Position(4));
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    public function invalid_movement_when_passing_wrong_player_id():void
    {
        Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),GameId::fromString(Uuid::uuid4()->toString()),PlayerId::fromString('-bbd8-dec21c9e5512'),new Position(4));
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    public function invalid_movement_when_passing_wrong_position():void
    {
        Movement::create(MovementId::fromString(Uuid::uuid4()->toString()),GameId::fromString('wrong id'),PlayerId::fromString(Uuid::uuid4()->toString()),new Position(67));
    }
}