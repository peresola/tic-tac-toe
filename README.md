# Tic Tac Toe

This project allows to run a tic tac toe game. It doesn't include any storage.

## Getting Started

Clone the repository into you machine.

### Prerequisites

You should have php and composer installed.

### Installing

Run composer install

```
composer install
```

From the command line you should be able to run the following commands:

Create user:
```
php bin/console app:create-user 63c29b4e-d2f7-11e7-9296-cec278b6b50a "peresola"
```
Delete user:
```
php bin/console app:delete-user 63c29b4e-d2f7-11e7-9296-cec278b6b50a
```
Make movement:
```
php bin/console app:make-movement 63c29b4e-d2f7-11e7-9296-cec278b6b50a 63c29b4e-d2f7-11e7-9296-cec278b6b50a 63c29b4e-d2f7-11e7-9296-cec278b6b50a 2
```
Start game:
```
php bin/console app:start-game 63c29b4e-d2f7-11e7-9296-cec278b6b50a 8d2a2046-9d32-4e86-8e17-51b28e9985d5 7782b2ec-b09c-42ae-bbd8-dec21c9e5512 
```
Check game has a winner:
```
php bin/console app:game-has-winner 63c29b4e-d2f7-11e7-9296-cec278b6b50a 
```
Check game has finished:
```
php bin/console app:game-has-finished 63c29b4e-d2f7-11e7-9296-cec278b6b50a
```

## Running the tests

```
php bin/phpunit
```

## Built With

* [Symfony](https://symfony.com/) - The web framework used
* [TacticianBundle](https://github.com/thephpleague/tactician-bundle) - Command Bus

## Authors

* **Pere Sola** - *Initial work* - [peresola](https://github.com/peresola)